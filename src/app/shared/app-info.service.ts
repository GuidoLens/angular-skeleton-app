import { Injectable } from "@angular/core";

import { environment } from "../../environments/environment";

@Injectable({
	providedIn: "root"
})
export class AppInfoService {
	title = "Login Page";
	version = "";
	environment = environment.production ? "prod" : "dev";
	isDebug = environment.debug;
}
