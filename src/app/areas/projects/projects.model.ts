
export type TagType = "new" | "test" | "test2" | "test3" | "test4";

export interface Project {
	key: string;
	title: string;
	description?: string;
	url: string;
	tag?: TagType;
}