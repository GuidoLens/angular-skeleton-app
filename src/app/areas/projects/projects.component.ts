import { Component } from "@angular/core";
import { Project } from "./projects.model";

@Component({
	selector: "app-projects",
	templateUrl: "./projects.component.html",
	styleUrls: ["./projects.component.scss"],
})
export class ProjectsComponent {
	projects: Project[] = [
		// js
		{
			key: "ssv-core",
			title: "Strategy 1",
			tag: "test",
			url: "https://github.com/sketch7/ssv-core",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
		},
		{
			key: "signalr-client",
			title: "Strategy 2",
			tag: "test",
			url: "https://github.com/sketch7/signalr-client",
			description: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto",
		},

		// ngx
		{
			key: "ngx-command",
			title: "Strategy 3",
			tag: "test",
			url: "https://github.com/sketch7/ngx.command",
			description:
				"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti",
		},
		{
			key: "angular-skeleton-app",
			title: "Strategy 4",
			tag: "test3",
			url: "https://github.com/sketch7/angular-skeleton-app",
			description: "omnis voluptas assumenda est",
		},
		{
			key: "ng2-heroes",
			title: "Strategy 5",
			tag: "test2",
			url: "https://github.com/sketch7/ng2-heroes",
			description: "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur",
		},

		// au
		{
			key: "ssv-au-core",
			title: "Strategy 6",
			tag: "new",
			url: "https://github.com/sketch7/ssv-au-core",
			description: "magni dolores eos qui ratione",
		},
		{
			key: "ssv-au-ui",
			title: "Strategy 7",
			tag: "new",
			url: "https://github.com/sketch7/ssv-au-ui",
			description: "blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui",
		},

		// csharp
		{
			key: "fluently-http-client",
			title: "Strategy 8",
			tag: "test",
			url: "https://github.com/sketch7/FluentlyHttpClient",
			description: "delectus, ut aut reiciendis voluptatibus maiores alias",
		},
	];
}
