import { Routes } from "@angular/router";

import { NavComponent } from "./nav/nav.component";
import { HomeComponent } from "./home/home.component";
import { ErrorComponent } from "./error/error.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { ProjectsComponent } from "./projects/projects.component";
import { ProjectComponent } from "./projects/project.component";
import { RegistrationComponent } from "./registration/registration.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";

export const AREAS_ROUTES: Routes = [
	{ path: "", component: HomeComponent, pathMatch: "full" },
	{ path: "projects", component: ProjectsComponent },
	{ path: "error", component: ErrorComponent },
	// { path: "**", component: NotFoundComponent },
	{ path: "registration", component: RegistrationComponent },
	{ path: "forgot", component: ForgotPasswordComponent },
];

export const AREAS_COMPONENTS = [
	NavComponent,

	// pages
	HomeComponent,
	ErrorComponent,
	NotFoundComponent,
	ProjectsComponent,
	ProjectComponent,
	RegistrationComponent,
	ForgotPasswordComponent,
];